# Prime List Game

# Prime function plucked as one of the many from:
# https://stackoverflow.com/questions/15285534/isprime-function-for-python-language
def is_prime(n):
	""""pre-condition: n is a nonnegative integer
	post-condition: return True if n is prime and False otherwise."""
	if n < 2: 
		return False;
	if n % 2 == 0:             
		return n == 2  # return False
	k = 3
	while k*k <= n:
		if n % k == 0:
			return False
		k += 2
	return True

def get_first_prime_after( n ):
	n = n + 1
	while not is_prime( n ) :
		n = n + 1
	return n

def guess_loop() :
	bad_try_count = 0
	list_primes = [ 2 ]
	i = 0
	while bad_try_count < 3 :
		player_number = int( input( "Enter the next number you think is a prime number: "))
		if is_prime( player_number ):
			print( "Yes, it's Prime.")
			if player_number == list_primes[ i ] :
				print( "Yes, it was the next one.")
				list_primes.append( get_first_prime_after( player_number ) )
				i += 1
			else :
				print( "Alas, it is not the next one.")
				bad_try_count += 1
		else :
			print( "Alas, not Prime.")
			bad_try_count += 1
	if bad_try_count >= 3 :
		print( "Sorry, you exceeded your allowed failure count.")

print( "Welcome to the Prime List Game")
print( "Your challenge is to state, one at a time, all the Prime numbers in sequence.")
print( "Remember that zero and one are not Prime.")
guess_loop()
print( "Thanks for playing, please try again sometime.")
